<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('bill_address');
            $table->integer('delivery_address')->nullable();
            $table->string('state');
            $table->text('note')->nullable();
            $table->integer('bill')->nullable();
            $table->timestamps();
            $table->foreign('bill_address')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('delivery_address')->references('id')->on('addresses')->onDelete('cascade');
        });

        Schema::create('cart_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->integer('product_id');
            $table->integer('quantity')->unsigned();
            $table->char('size');
            $table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('pictures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
