<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'CoreController@index')->name('home');
Route::get('/contact', 'CoreController@contact')->name('contact');
Route::Post('/contact', 'CoreController@saveContact')->name('saveContact');

Route::get('/about', 'CoreController@about')->name('about');
Route::get('/termsOfSales', 'CoreController@termsOfSales')->name('termsOfSales');
Route::get('/deliveryInformations', 'CoreController@deliveryInfo')->name('deliveryInfo');
Route::get('/paymentInformations', 'CoreController@paymentInfo')->name('paymentInfo');
//Route::get('/termsOfSales', 'CoreController@termsOfSales')->name('');
//Route::resource('Product', 'ProductsController');

Route::get('/bracelets/{id_category?}', 'StoreController@index')->name('bracelets');

Route::get('/product/{name}', 'StoreController@product')->name('product');

Route::get('/cart', 'CartController@index')->name('cart');
Route::get('/cart/checkout', 'CartController@checkout')->name('checkout');
Route::post('/cart/check', 'CartController@check')->name('check');

Route::get('/cart/payment/{cart_id}', 'CartController@payment')->name('cart.payment');
Route::post('/cart/paymentCheckout/{cart_id}', 'CartController@paymentCheckout')->name('cart.paymentCheckout');
Route::post('/cart/transaction/{cart_id}', 'CartController@transaction')->name('cart.transaction');

Route::get('/cart/recap/{cart_id}', 'CartController@recap')->name('cart.recap');



