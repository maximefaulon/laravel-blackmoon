<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ablette
 * Date: 14/05/2018
 * Time: 14:50
 */

namespace App;


use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Transaction;

class PaypalTransactions
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function get() {
        $list = new ItemList();
        $price = 0.00;
        foreach ($this->products as $product) {
            $item = (new Item())
                ->setName($product->name)
                ->setPrice($product->price)
                ->setCurrency("EUR")
                ->setQuantity($product->pivot->quantity);
            $price += $product->price*$product->pivot->quantity;
            $list->addItem($item);
        }



        $details = (new Details())
            ->setSubtotal($price);

        $amount = (new Amount())
            ->setTotal($price)
            ->setCurrency("EUR")
            ->setDetails($details);

        return $transaction = (new Transaction())
            ->setItemList($list)
            ->setDescription("Achat sur bleueisland.com")
            ->setAmount($amount);

    }
}