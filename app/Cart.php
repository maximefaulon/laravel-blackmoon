<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    private $delivery_address = null;

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity', 'size');
    }
}
