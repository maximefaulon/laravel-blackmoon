<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Product;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class CoreController extends Controller
{
    public function index()
    {
//        $token = "0b3a027ef4934701951d51d5dcdf8f34";
//        $endpoint  = "https://api.instagram.com/v1/media/search?lat=48.858844&lng=2.294351&access_token=$token";
//        $curl = curl_init($endpoint);
//        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//
//        $data = curl_exec($curl);
//        $json = json_decode($data);
//
//        dd($json);
        $products = Product::all()->take(10);
        return view('core.index', [ 'products' => $products]);
    }

    public function contact (FormBuilder $formBuilder) {
        $form = $formBuilder->create(\App\Forms\ContactsForm::class, [
            'method' => 'POST',
            'url' => route('contact')
        ]);

        return view('core.contact', compact('form'));
    }

    public function saveContact (FormBuilder $formBuilder) {
        $contact = new Contact;
        $form = $formBuilder->create(\App\Forms\ContactsForm::class, [
            'method' => 'POST',
            'url' => route('contact')
        ]);

        $form->redirectIfNotValid();

        $contact->setAttribute('name', $form->getField('name')->getRawValue());
        $contact->setAttribute('firstname', $form->getField('firstname')->getRawValue());
        $contact->setAttribute('email', $form->getField('email')->getRawValue());
        $contact->setAttribute('content', $form->getField('content')->getRawValue());

        $contact->save();
        return redirect()->route('home');
    }

    public function termsOfSales () {
        return view('core.termsOfSales');
    }

    public function paymentInfo () {
        return view('core.paymentInfo');
    }

    public function deliveryInfo () {
        return view('core.deliveryInfo');
    }

    public function about () {
        return view('core.about');
    }
}
