<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index ($id_category = null) {
        $name = "Bracelet ";
        if($id_category){
            $category = Category::where('name', $id_category)->first();
            $products = $category->products;
            $name = $name . $category->name;
            $desc = $category->description;
        }
        else {
            $products = Product::orderBy('id', 'asc')->get();
            $desc = "Tous nos bracelets homme.  Bracelets ancre, bracelet mousqueton ou encore bracelet queue de baleine. ";
        }
        return view('store.bracelets', [ 'products' => $products, 'name' => $name, 'description' => $desc]);
    }

    public function product ($name) {
        $product = Product::where('name', $name)->first();
        return view('store.product', [ 'product' => $product]);
    }
}
