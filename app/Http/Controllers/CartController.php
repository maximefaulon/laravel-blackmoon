<?php

namespace App\Http\Controllers;

use App\Address;
use App\Cart;
use App\PaypalTransactions;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Kris\LaravelFormBuilder\FormBuilder;
use function MongoDB\BSON\toJSON;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CartController extends Controller
{
    public function index () {
       return view('cart.index');
    }

    public function checkout (FormBuilder $formBuilder) {
        $form = $formBuilder->create(\App\Forms\CartsForm::class, [
            'method' => 'POST',
            'url' => route('check')
        ]);
        return view('cart.checkout', compact('form'));
    }

    public function check (FormBuilder $formBuilder, Request $request) {
        $cart = new Cart();
        $delivery_address = new Address();
        $bill_address = new Address();
        $form = $formBuilder->create(\App\Forms\CartsForm::class, [
            'method' => 'POST',
            'url' => route('checkout')
        ]);
        $form->redirectIfNotValid();
        $bill_address->setAttribute('name', $form->getField('bill_address')->getRawValue()['name']);
        $bill_address->setAttribute('firstname', $form->getField('bill_address')->getRawValue()['firstname']);
        $bill_address->setAttribute('route', $form->getField('bill_address')->getRawValue()['route']);
        $bill_address->setAttribute('postalCode', $form->getField('bill_address')->getRawValue()['postalCode']);
        $bill_address->setAttribute('city', $form->getField('bill_address')->getRawValue()['city']);
        $bill_address->setAttribute('country', $form->getField('bill_address')->getRawValue()['country']);
        $bill_address->setAttribute('phone', $form->getField('bill_address')->getRawValue()['phone']);
        $bill_address->setAttribute('type', 'bill');

        $bill_address->save();

        if( $form->getField('delivery_address')->getRawValue()['name'] ) {
            $delivery_address->setAttribute('name', $form->getField('delivery_address')->getRawValue()['name']);
            $delivery_address->setAttribute('firstname', $form->getField('delivery_address')->getRawValue()['firstname']);
            $delivery_address->setAttribute('route', $form->getField('delivery_address')->getRawValue()['route']);
            $delivery_address->setAttribute('postalCode', $form->getField('delivery_address')->getRawValue()['postalCode']);
            $delivery_address->setAttribute('city', $form->getField('delivery_address')->getRawValue()['city']);
            $delivery_address->setAttribute('country', $form->getField('delivery_address')->getRawValue()['country']);
            $delivery_address->setAttribute('phone', $form->getField('delivery_address')->getRawValue()['phone']);
            $delivery_address->setAttribute('type', 'delivery');

            $delivery_address->save();
            $cart->setAttribute('delivery_address', $delivery_address->getAttribute('id'));
        }
        else {
            $cart->setAttribute('delivery_address', $bill_address->getAttribute(('id')));
        }

        $cart->setAttribute('email', $form->getField('email')->getRawValue());
        $cart->setAttribute('bill_address', $bill_address->getAttribute('id'));
        $cart->setAttribute('state', 'curent');
        $cart->setAttribute('note', $form->getField('note')->getRawValue());
        $cart->save();

        $products = json_decode(base64_decode($_COOKIE['cartArticles']));
        if ($products == null) {
            return redirect()->route('home');
        }
        foreach ( $products as $product) {
            $cart->products()->attach(Product::where('name', $product->name)->first(), [ 'size' => $product->size, 'quantity' => $product->qt ]);
        }
//        dd($cart->getAttribute('id'));
        return redirect()->route('cart.payment', [ 'id' => $cart->getAttribute('id')]);
    }

    public function payment ($cart_id) {
        $cart = Cart::find($cart_id);
        $products = $cart->products;
        return view('cart.payment', [ 'products' => $products, 'cart_id' => $cart_id ]);
    }

    public function paymentCheckout ($cart_id) {
        $cart = Cart::find($cart_id);
        $products = $cart->products;

        if ($products == null) {
            return redirect()->route('home');
        }

        $paypalTransaction = new PaypalTransactions($products);
        $transaction = $paypalTransaction->get();

        $id = env('PAYPAL_ID');
        $secret = env('PAYPAL_SECRET');

        $apiContext = new ApiContext(
            new OAuthTokenCredential($id, $secret)
        );

        $apiContext->setConfig(array('mode' => 'live'));

        $payment = new Payment();
        $payment->setIntent('sale');

        $redirectUrls = (new RedirectUrls())
            ->setReturnUrl(route('cart.payment', ['cart_id' => $cart_id]))
            ->setCancelUrl(route('home'));

        $payment->setRedirectUrls($redirectUrls);
        $payment->setPayer((new Payer())->setPaymentMethod('paypal'));

        $payment->addTransaction($transaction);
        try {
            $payment->create($apiContext);
//            $link = $payment->getApprovalLink();
            $link =  [
                "id" => $payment->getId()
            ];
        } catch (PayPalConnectionException $e) {
            return var_dump(json_decode($e->getData()));
        }

        return response()->json($link);
    }

    public function transaction ($cart_id, Request $request) {
        $cart = Cart::find($cart_id);
        $products = $cart->products;

        $paypalTransaction = new PaypalTransactions($products);
        $transaction = $paypalTransaction->get();

        $paymentID= $request->request->get('paymentID');
        $payerID = $request->request->get('payerID');


        $id = env('PAYPAL_ID');
        $secret = env('PAYPAL_SECRET');

        $apiContext = new ApiContext(
            new OAuthTokenCredential($id, $secret)
        );

        $apiContext->setConfig(array('mode' => 'live'));

        $payment = Payment::get($paymentID, $apiContext);

        $execution = (new PaymentExecution())
            ->setPayerId($payerID)
            ->addTransaction($transaction);

        try {
            $payment->execute($execution, $apiContext);
            $cart->setAttribute('state', 'pay');
            $cart->save();
        }catch (PayPalConnectionException $e) {
            header('500 Internal error', true, 500);
            return new Response(var_dump(json_decode($e->getData())));
        }

        /*$basket->setEmail($email);
        $basket->setTransactionNumber($paymentID);
        $em->persist($basket);
        $em->flush();*/

        return response('');
    }

    public function recap ($cart_id) {
        $cart = Cart::find($cart_id);
        if($cart->state == 'pay') {
            Mail::send('emails.recap',[], function ($message){
                $message->to('maxime.faulon@gmail.com')->from('contact@bleueisland.com')->subject('Nouvelle commande');
            });
            return view('cart.recap');
        }
        else{
            return redirect()->route('home');
        }
    }
}
