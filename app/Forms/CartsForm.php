<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CartsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('email', 'email', [
                'rules' => 'required|min:5|email',
                'label' => 'Adresse mail'
            ])
            ->add('bill_address', 'form', [
                'label' => 'Adresse',
                'class' => $this->formBuilder->create('App\Forms\AddressBillForm')
            ])
            ->add('delivery_address', 'form', [
                'label' => 'Adresse de livraison',
                'class' => $this->formBuilder->create('App\Forms\AddressDeliveryForm')
            ])
            ->add('note', 'textarea')
            ->add('submit', 'submit', ['label' => 'payer']);
//            ->add('firstname', 'text')
//            ->add('email', 'email')
//            ->add('content', 'textarea');
    }
}
