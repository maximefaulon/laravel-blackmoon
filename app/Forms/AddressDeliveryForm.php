<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class AddressDeliveryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text',[
                'label' => 'Nom'
            ])
            ->add('firstname', 'text',[
                'label' => 'Prénom'
            ])
            ->add('route', 'text',[
                'label' => 'Adresse'
            ])
            ->add('postalCode', 'text',[
                'label' => 'Code postal'
            ])
            ->add('city', 'text',[
                'label' => 'Ville'
            ])
            ->add('country', 'text',[
                'label' => 'Pays'
            ])
            ->add('phone', 'text',[
                'label' => 'Téléphone'
            ]);
    }
}
