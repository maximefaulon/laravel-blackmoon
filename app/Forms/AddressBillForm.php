<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class AddressBillForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text',[
                'rules' => 'required|min:3',
                'label' => 'Nom'
            ])
            ->add('firstname', 'text',[
                'rules' => 'required|min:3',
                'label' => 'Prénom'
            ])
            ->add('route', 'text',[
                'rules' => 'required|min:5',
                'label' => 'Adresse'
            ])
            ->add('postalCode', 'text',[
                'rules' => 'required|min:5|numeric',
                'label' => 'Code postal'
            ])
            ->add('city', 'text',[
                'rules' => 'required|min:3',
                'label' => 'Ville'
            ])
            ->add('country', 'text',[
                'rules' => 'required|min:5',
                'label' => 'Pays'
            ])
            ->add('phone', 'text',[
                'rules' => 'required|min:5|numeric',
                'label' => 'Téléphone'
            ]);
    }
}
