<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ContactsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text',[
                'rules' => 'required|min:3',
                'label' => 'Nom'
            ])
            ->add('firstname', 'text',[
                'rules' => 'required|min:3',
                'label' => 'Prénom'
            ])
            ->add('email', 'email',[
                'rules' => 'required|email|min:5',
                'label' => 'Adresse mail'
            ])
            ->add('content', 'textarea',[
                'rules' => 'required|min:5',
                'label' => 'Message'
            ])
            ->add('submit', 'submit',[
                'label' => 'Envoyer'
            ]);
    }
}
