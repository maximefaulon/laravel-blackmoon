@extends('layouts.app')

@section('title')
    Informations paiement - Bracelets BlackMoon
@endsection

@section('content')
    <main class="container mb-5 mt-5">
        <h1 class="display-3 font-weight-light text-center">Informations de paiement</h1>
        <p class="text-muted mt-4">Le paiement s'éffectue via paypal, par compte ou par carte bancaire, le paiement est donc 100% sécurisé.
            <br> Après avoir rempli le formulaire pour la facturation et la livraison, une page avec le bouton paypal vous est alors proposé,
            vous n'avez plus qu'à cliquer sur le bouton, alors une fenêtre paypal s'ouvre, vous pouvez alors rentrer vos identifiants ou alors payer par carte. Un email de confirmation vous sera alors envoyé automatiquement.</p>
    </main>
@endsection