@extends('layouts.app')

@section('title')
    Bracelet homme corde en acier inoxydable | ancre marine
@endsection

@section('meta')
    <meta name="description" content="Bracelet homme de qualité supérieure. En acier inoxydable et paracorde. Fabrication Française. Ancre marine, mousqueton ou queue de baleine." />
@endsection

@section('content')
    {{--<main>--}}
    {{--<div class="hero--home">--}}
    {{--<img class="img-responsive w-100" src="{{ asset('images/Accueil-BlackMoon3.jpg')  }}" alt="">--}}
    {{--<div class="hero__content col-7 offset-md-2">--}}
    {{--<h1 class="display-4">Nos bracelets BlackMoon</h1>--}}
    {{--<p class="lead">Bracelet homme marin</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="mt-2">--}}
    {{--<div class="row no-gutters justify-content-center">--}}
    {{--<div class="col-12 col-md-7">--}}
    {{--<img class="img-responsive w-100 rounded" src="{{ asset('images/accueil-bm.jpg')  }}" alt="">--}}
    {{--</div>--}}
    {{--<div class="col-12 col-md-5 p-4 align-self-center">--}}
    {{--<h1 class="display-4 text-center">Bracelets homme</h1>--}}
    {{--<p class="lead">--}}
    {{--Nos bracelets ont été pensés pour les amoureux de la nature, destinés à devenir un accessoire pour toutes occasions.--}}
    {{--<br>Il est souple et léger ce qui permet de le porter tout le temps et sans limite.--}}
    {{--Bracelet ancre, mousqueton ou queue de baleine, tous ces modèles sont conçus pour vous plaire et surtout être un bracelet tendance.--}}

    {{--</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</main>--}}


    {{--<div class="banner">--}}
        {{--<!-- start slider -->--}}
        {{--<div id="fwslider">--}}
            {{--<div class="slider_container">--}}
                {{--<div class="slide">--}}
                    {{--<!-- Slide image -->--}}
                    {{--<img src="{{asset('images/IMG_7698.JPG')}}" class="img-responsive" alt=""/>--}}
                    {{--<!-- /Slide image -->--}}
                    {{--<!-- Texts container -->--}}
                    {{--<div class="slide_content">--}}
                        {{--<div class="slide_content_wrap">--}}
                            {{--<!-- Text title -->--}}
                            {{--<h1 class="title">Bracelet homme<br>BlackMoon</h1>--}}
                            {{--<!-- /Text title -->--}}
                            {{--<div class="button text-right"><a href="{{ route('bracelets') }}">Voir produits</a></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /Texts container -->--}}
                {{--</div>--}}
                {{--<!-- /Duplicate-->--}}
                {{--<div class="slide">--}}
                    {{--<img src="{{ asset('images/IMG_6995.JPG') }}" class="img-responsive" alt=""/>--}}
                    {{--<div class="slide_content">--}}
                        {{--<div class="slide_content_wrap">--}}
                            {{--<h2 class="title">L'accessoire tendance<br>et classe</h2>--}}
                            {{--<div class="button text-right"><a href="{{ route('bracelets') }}">Voir produits</a></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!--/slide -->--}}
            {{--</div>--}}
            {{--<div class="timers"></div>--}}
            {{--<div class="slidePrev"><span></span></div>--}}
            {{--<div class="slideNext"><span></span></div>--}}
        {{--</div>--}}
        {{--<!--/slider -->--}}
    {{--</div>--}}
    {{--<ul id="lightSlider">--}}

        {{--@foreach( $products as $product)--}}
            {{--<a href="{{ route('product', [ 'id' => $product->name]) }}">--}}
                {{--<li data-thumb="{{ asset($product->pictures[0]->path) }}">--}}
                    {{--<a href="{{ route('product', [ 'id' => $product->name]) }}">--}}
                    {{--<img src="{{ asset($product->pictures[0]->path) }}" width="300px" />--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</a>--}}
        {{--@endforeach--}}
    {{--</ul>--}}
    <main>
        <section class="hero--home">
            <div class="hero__wrap">
                <div class="hero__content d-none d-md-block">
                    <p class="text-uppercase">Bracelet <span class="text-uppercase">bleueisland</span></p>
                    <p>Bracelet de qualité supérieur</p>
                    <a class="btn btn-secondary w-50 mt-3" href="{{ route('bracelets') }}">
                        Nos bracelets homme
                    </a>
                    {{--<button class="btn btn-secondary w-50 mt-3">Notre collection</button>--}}
                </div>
            </div>
        </section>
        <section style="margin-top: 120px; margin-bottom: 120px;" class="container">
            <div class="section--desc my-5 row no-gutters justify-content-around">
                <div class="col-12 col-md-5 mb-4 mb-md-0 align-self-center">
                    <h1 class="header">Un bracelet homme corde noir tendance</h1>
                    <p class="subtext">
                        Nos bracelets homme en <strong>acier inoxydable</strong> et <strong>paracorde</strong> vont vous plaire jusqu'au bout de leurs finitions. Alliant moderne, solidité et légèreté,
                        il sera votre accessoire indispensable dont vous ne voudrez plus vous séparer.
                        Ca tombe bien puisque nous avons conçu notre gamme pour qu'elle aille en toutes occasions.
                        Le <strong>bracelet homme paracorde</strong> est dans l'air du temps, que ce soit le bracelet acier ancre, le bracelet acier queue de baleine ou encore le bracelet acier mousqueton.
                        <br>
                        Les bracelets homme blackmoon sont faits pour perdurer dans le temps.
                        Dans notre collection, vous allez trouver votre bonheur, bracelet homme or, bracelet homme argent, <strong>bracelet homme ancre marine</strong>, bracelet queue de baleine, bracelet mousqueton.
                        Une large gamme de couleur, qui est en constante évolution.
                        Une finition choisie pour aller avec tous types de style.
                    </p>
                </div>
                <div class="col-12 col-md-7 text-center">
                    <img class="img-responsive w-75" src="./images/IMG-7291.JPG" alt="Bracelet homme acier">
                </div>

            </div>
        </section>
        <section class="slider--home container">
            <header>
                <h2>Nos bracelets homme</h2>
            </header>
            <ul id="lightSlider">
                @foreach( $products as $product)
                    {{--<a href="{{ route('product', [ 'id' => $product->name]) }}">--}}
                    <li data-thumb="{{ asset($product->pictures[0]->path) }}">
                        <a href="{{ route('product', [ 'id' => $product->name]) }}">
                            <img src="{{ asset($product->pictures[0]->path) }}" alt="{{ $product->nameref }}" style="max-width: 100%"/>
                            <div class="lightSlider__content">
                                <h5 style="font-size: 15px">{{ $product->name }} - {{ $product->nameref }}</h5>
                                <p>{{ $product->price }}€</p>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
            {{--<h3>Les bracelets tendances</h3>--}}
        </section>
    </main>
    {{--<div class="main">--}}
        {{--<div class="content-top">--}}
            {{--<h2>Bracelets homme</h2>--}}
            {{--<ul id="lightSlider">--}}
                {{--@foreach( $products as $product)--}}
                    {{--<a href="{{ route('product', [ 'id' => $product->name]) }}">--}}
                    {{--<li data-thumb="{{ asset($product->pictures[1]->path) }}">--}}
                        {{--<a href="{{ route('product', [ 'id' => $product->name]) }}">--}}
                            {{--<img src="{{ asset($product->pictures[1]->path) }}" style="max-width: 100%"/>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
            {{--<h3>Les bracelets tendances</h3>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="content-bottom mb-5">--}}
        {{--<div class="container">--}}
            {{--<div class="row content_bottom-text">--}}
                {{--<div class="col-12 col-md-7 offset-0 offset-md-4 pt-0 pt-md-3">--}}
                    {{--<h3>BlackMoon<br>Bracelet de qualité</h3>--}}
                    {{--<p class="m_1 pt-0 pt-md-5">Les bracelets blackmoon sont des accessoires incontournables pour avoir un look dans l'air du temps.<br>--}}
                    {{--Nous avons comme but de vous fournir un bracelet de qualité supérieur avec des matériaux de qualité, que ce soit le bracelet ancre,--}}
                        {{--le bracelet mouqueton, ou encore le bracelet queue de baleine, des fermoirs en acier inoxydable qui resisteront à toutes les occasions.--}}
                        {{--Nous essayons de parfaire au mieux les couleurs avec les fermoirs pour créer une harmonie.--}}
                    {{--</p>--}}
                    {{--<p class="m_2">Jeune étudiant amoureux des beaux bracelets, je voulais donner à ma gamme de bracelet une inspiration de la nature, de la mer ainsi que le goût de l'aventure.--}}

                        {{--<br>Tous nos modèles sont montés en France.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="features">--}}
    {{--<div class="container">--}}
    {{--<h3 class="m_3">Features</h3>--}}
    {{--<div class="close_but"><i class="close1"> </i></div>--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-3 top_box">--}}
    {{--<div class="view view-ninth"><a href="single.html">--}}
    {{--<img src="images/pic1.jpg" class="img-responsive" alt=""/>--}}
    {{--<div class="mask mask-1"> </div>--}}
    {{--<div class="mask mask-2"> </div>--}}
    {{--<div class="content">--}}
    {{--<h2>Hover Style #9</h2>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<h4 class="m_4"><a href="#">nostrud exerci ullamcorper</a></h4>--}}
    {{--<p class="m_5">claritatem insitam</p>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 top_box">--}}
    {{--<div class="view view-ninth"><a href="single.html">--}}
    {{--<img src="images/pic2.jpg" class="img-responsive" alt=""/>--}}
    {{--<div class="mask mask-1"> </div>--}}
    {{--<div class="mask mask-2"> </div>--}}
    {{--<div class="content">--}}
    {{--<h2>Hover Style #9</h2>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>--}}
    {{--</div>--}}
    {{--</a> </div>--}}
    {{--<h4 class="m_4"><a href="#">nostrud exerci ullamcorper</a></h4>--}}
    {{--<p class="m_5">claritatem insitam</p>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 top_box">--}}
    {{--<div class="view view-ninth"><a href="single.html">--}}
    {{--<img src="images/pic3.jpg" class="img-responsive" alt=""/>--}}
    {{--<div class="mask mask-1"> </div>--}}
    {{--<div class="mask mask-2"> </div>--}}
    {{--<div class="content">--}}
    {{--<h2>Hover Style #9</h2>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>--}}
    {{--</div>--}}
    {{--</a> </div>--}}
    {{--<h4 class="m_4"><a href="#">nostrud exerci ullamcorper</a></h4>--}}
    {{--<p class="m_5">claritatem insitam</p>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 top_box1">--}}
    {{--<div class="view view-ninth"><a href="single.html">--}}
    {{--<img src="images/pic4.jpg" class="img-responsive" alt=""/>--}}
    {{--<div class="mask mask-1"> </div>--}}
    {{--<div class="mask mask-2"> </div>--}}
    {{--<div class="content">--}}
    {{--<h2>Hover Style #9</h2>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>--}}
    {{--</div>--}}
    {{--</a> </div>--}}
    {{--<h4 class="m_4"><a href="#">nostrud exerci ullamcorper</a></h4>--}}
    {{--<p class="m_5">claritatem insitam</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <section class="container text-center">
        <a href="https://www.instagram.com/bleueisland/" class="display-3 mb-2 insta-title">@bleueisland</a>
        <div class="mb-3" id="instafeed"></div>
    </section>
@endsection
@section('scripts')
    {{--<script type="text/javascript">--}}
        {{--var feed = new Instafeed({--}}
            {{--get: 'user',--}}
            {{--// tagName: 'love',--}}
            {{--clientId: 'b17a5a56176e48ebaae103e58fd44d50'--}}
        {{--});--}}
        {{--feed.run();--}}
    {{--</script>--}}
@endsection

