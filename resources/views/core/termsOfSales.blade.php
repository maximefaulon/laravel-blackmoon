@extends('layouts.app')

@section('title')
    CGV - Bracelets homme BlackMoon
@endsection

@section('content')
    <main class="container mb-5 mt-5">
            <h1 class="display-4">Conditions générales de ventes</h1>
            <p class="lead">
                Les présentes conditions de vente sont conclues entre la partie Monsieur Faulon Maxime, auto-entrepreneur
                {{--inscrit ____________ ,--}},
                son adresse postale 20 Rue Jean Sebastien Bach et gère le site internet ablackmoon.com, et par toute personne physique ou morale
                souhaitant procéder à un achat par le site internet.
            </p>
            <h2 class="">Clause n° 1 : Objet</h2>
            <p class="lead">Les conditions générales de vente décrites ci-après détaillent les droits et obligations de Monsieur Faulon Maxime et de la partie
                qui sera liée dans le cadre de la vente en ligne.
                Tout contrat de vente, entre le client et Monsieur Faulon Maxime impliquera l'adhésion sans réserve du client aux présentes conditions
                générales de vente, déterminé par l’acceptation claire et non équivoque des conditions générales de ventes, et faisant office de signature
                numérique du contrat.
                Monsieur Faulon Maxime peut à tout moment modifier ces conditions de ventes, lors de nouvelles réglementations ou dans le but d'améliorer
                l’utilisation de son site. De ce fait, les conditions applicables seront celles en vigueur à la date de la commande par le client.</p>
            <h2 class="">Clause n° 2 : Produits</h2>
            <p class="lead">Les produits mis en ligne sur le site internet ablackmoon.com appartiennent à Monsieur Faulon Maxime, et ce, jusqu’au transfert de propriété du produit.
                Chaque produit est présenté sur le site internet sous forme d’un descriptif reprenant ses principales caractéristiques techniques. L’auteur se réserve
                le droit de modification, de mise à jour et de suppression des produits. Les photographies sont les plus fidèles possibles mais n’engagent en rien
                Monsieur Faulon Maxime, la représentation visuelle étant propre à chacun. La vente des produits présents sur le site ablackmoon.com ne se fera qu’avec des
                parties ayant la pleine capacité juridique.</p>
            <h2 class="">Clause n°3 : Tarifs</h2>
            <p class="lead">Le prix catalogué des produits est présenté en Euros (€).
                Les prix indiqués ne prennent pas en compte les frais de livraison. Ils seront facturés en supplément du prix des produits achetés suivant le montant
                total de la commande. En France métropolitaine les frais de port sont offerts.
            <h2 class="">Clause n° 4 : Remises, Escomptes, Ristournes et Rabais</h2>
            <p class="lead">
                Les tarifs proposés comprennent les remises que Monsieur Faulon Maxime serait amené à octroyer. Aucun escompte ne sera consenti en cas de
                paiement anticipé. De même que, aucune ristourne ne sera effectuée, la vente de bijoux aux particuliers ne s’y prêtant pas.</p>
            En cas de produit défectueux, le produit sera echangé.
            </p>

            <h2 class="">Clause n° 5 : Commande et modalités de paiement</h2>
            <p class="lead">
                Avant toute commande, l’acheteur doit créer un compte sur le site ablackmoon.com
                Le règlement des commandes s'effectue :
                · soit par carte bancaire,
                · soit par un prestataire de paiement (Paypal)

                L’acheteur choisit le mode de paiement de son choix. L’étape suivante lui propose de vérifier l’ensemble des
                informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case
                correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ».
                Enfin, l’acheteur est redirigé sur l’interface sécurisée PAYPAL ou carte bleue afin de renseigner en toute
                sécurité ses références de compte Paypal ou de carte bleue personnelle. Si le paiement est accepté, la
                commande est enregistrée et le contrat définitivement formé. Le paiement par compte Paypal ou par carte
                bancaire est irrévocable.
                En cas d’utilisation frauduleuse de celle-ci, l’acheteur pourra exiger l’annulation du paiement par carte, les
                sommes versées seront alors recréditées ou restituées. La responsabilité du titulaire d’une carte bancaire
                n’est pas engagée si le paiement contesté a été prouvé effectué frauduleusement, à distance, sans utilisation
                physique de sa carte. Pour obtenir le remboursement du débit frauduleux et des éventuels frais bancaires que
                l’opération a pu engendrer, le porteur de la carte doit contester, par écrit, le prélèvement auprès de sa
                banque, dans les 70 jours suivant l’opération, voire 120 jours si le contrat le liant à celle-ci le prévoit.
                Les montants prélevés sont remboursés par la banque dans un délai maximum d’un mois après réception de la
                contestation écrite formée par le porteur. Aucun frais de restitution des sommes ne pourra être mis à la charge
                du titulaire.
                La confirmation d’une commande entraîne acceptation des présentes conditions de vente, la reconnaissance d’en avoir
                parfaite connaissance et la renonciation à se prévaloir de ses propres conditions d’achat. L’ensemble des données fournies
                et la confirmation enregistrée vaudront preuve de la transaction. La confirmation de l’enregistrement de sa
                commande sera communiquée par courrier électronique.
            </p>
            <h2 class="">Article n°6 : Réserve de propriété</h2>
            <p class="lead">
                Faulon Maxime conserve la propriété pleine et entière des produits vendus jusqu'au parfait
                encaissement du prix, frais et taxes compris.
            </p>
            <h2 class="">Article n°7 : Rétractation</h2>
            <p class="lead">
                En vertu de l’article L121-20 du Code de la consommation, l’acheteur
                dispose d'un délai de quatorze jours ouvrables à compter de la livraison de leur commande pour exercer
                son droit de rétractation et ainsi faire retour du produit au vendeur pour échange ou remboursement
                sans pénalité, à l’exception des frais de retour.
            </p>
            <h2 class="">Clause n° 8 : Livraison</h2>
            <p class="lead">
                Les livraisons seront effectuées à l’adresse indiquée sur le bon de commande.
                Les commandes sont effectuées par La Poste via COLISSIMO, ____________ service de livraison avec suivi,
                dont le numéro du suivi vous sera communiqué, remise sans signature.
                Les délais de livraison ne sont donnés qu’à titre indicatif, tout retard raisonnable dans la livraison des
                produits ne pourra pas donner lieu à un remboursement. Cependant, si ceux-ci dépassent ______ à compter
                de la commande, le contrat de vente pourra être résilié et l’acheteur remboursé.
                Les risques liés au transport sont à la charge de l'acquéreur à compter du moment où les articles
                quittent les locaux de Faulon Maxime. L’acheteur est tenu de vérifier en présence du préposé de La
                Poste ou du livreur, l’état de l’emballage de la marchandise. En cas de dommage pendant le transport,
                toute protestation doit être effectuée auprès du transporteur dans un délai de trois jours à compter de
                la livraison.
            </p>
            <h2 class="">Clause n° 9 : Force majeure</h2>
            <p class="lead">
                La responsabilité de Mr Faulon Maxime ne pourra pas être mise en oeuvre si la non-exécution ou
                le retard dans l'exécution de l'une de ses obligations décrites dans les présentes conditions
                générales de vente découle d'un cas de force majeure. À ce titre, la force majeure s'entend de tout
                événement extérieur, imprévisible et irrésistible au sens de l'article 1148 du Code civil.
            </p>
            <h2 class="">Article 10 : Garantie</h2>
            <p class="lead">
                Tous les produits fournis par Mr Faulon Maxime bénéficient de la garantie légale prévue par
                les articles 1641 et suivants du Code civil. En cas de non conformité d’un produit vendu,
                il pourra être retourné à la société à l’adresse indiquée pour un échange ou un remboursement.
                Toutes les réclamations, demandes d’échange ou de remboursement doivent s’effectuer par mail à
                l’adresse suivante : contact@ablackmoon.com , dans un délai de_________ après livraison.

            <h2 class="">Article 11 : Responsabilité</h2>
            <p class="lead">
                Mr Faulon Maxime, dans le processus de vente à distance, n’est tenue
                que par une obligation de moyens. Sa responsabilité ne pourra être engagée pour un
                dommage résultant de l’utilisation du réseau Internet tel que perte de données, intrusion,
                virus, rupture du service, ou autres problèmes involontaires.
            </p>
            <h2 class="">  Article 12 : Propriété intellectuelle</h2>
            <p class="lead">
                Tous les éléments du site ablackmoon.com sont et restent la propriété intellectuelle et exclusive de
                Mr Faulon Maxime. Personne n’est autorisé à reproduire, exploiter, ou utiliser à quelque titre que ce
                soit, même partiellement, des éléments du site qu’ils soient sous forme de photo, logo, visuel ou texte.
            </p>


            <h2 class=""> Article 13. Données à caractère personnel</h2>
            <p class="lead">
                Mr Faulon Maxime s'engage à préserver la confidentialité des informations fournies par l’acheteur,
                qu'il serait amené à transmettre pour l'utilisation de certains services. Toute information le
                concernant est soumise aux dispositions de la loi n° 78-17 du 6 janvier 1978. A ce titre, l'internaute
                dispose d'un droit d'accès, de modification et de suppression des informations le concernant.
                Il peut en faire la demande à tout moment en se connectant à son compte sur le site ablackmoon.com ou par mail
                à l’adresse contact@ablackmoon.com.
            </p>
            <h2 class="">Clause n° 14 : Tribunal compétent</h2>

            <p class="lead">
                Tout litige relatif à l'interprétation et à l'exécution des présentes conditions
                générales de vente est soumis au droit français.
                À défaut de résolution amiable, le litige sera porté devant le Tribunal de
                commerce Montpellier.
            </p>
        </div>
    </main>
@endsection