@extends('layouts.app')

@section('title')
    Contact - Bracelets BlackMoon
@endsection

@section('content')
    <main class="container-fluid mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                {{--<form>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="inputName">Nom</label>--}}
                        {{--<input type="text" class="form-control" id="inputName" placeholder="Votre nom">--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="Email">Email</label>--}}
                        {{--<input type="email" class="form-control" id="Email" placeholder="Votre email">--}}
                    {{--</div>--}}
                    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                {{--</form>--}}
                {!! form($form) !!}
            </div>

        </div>
    </main>
@endsection