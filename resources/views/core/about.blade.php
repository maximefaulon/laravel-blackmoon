@extends('layouts.app')

@section('title')
    A propos - Bracelets homme BlackMoon
@endsection

@section('content')
    <div class="content-top p5">
        <h1>Notre histoire</h1>
    </div>
    <main class="container mb-5 mt-5">
        <div class="row">
            <div class="col-12 col-md-6 align-self-center">
                <h2 class="display-4">Les bracelets bleueisland</h2>
                <p>Chaque bracelet bleueisland est fabriqué à la main avec des produits de qualité pour qu'il soit confortable à porter, élégant et solide dans le temps.
                C'est un accessoire idéal pour apporter une touche originale ou souligner un style.
                Notre gamme de couleur et de fermeture permet de le porter avec une tenue décontractée ou habillée, pour un look moderne qui accroche le regard.

                Parcourez notre collection pour trouver votre bracelet homme qu'il vous faut.</p>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/category1.JPG" class="w-100" alt="Bracelet homme bleueisland">
            </div>
        </div>
    </main>
@endsection