@extends('layouts.app')

@section('title')
    Informations livraison - Bracelets BlackMoon
@endsection

@section('content')
    <main class="container mb-5 mt-5">
        <h1 class="display-3 font-weight-light text-center">Informations livraison</h1>
        <p class="text-muted mt-4">Nous expédions votre commande dans un délai de 48h ouvrés. Les frais de port sont offerts en france métropolitaine.
            <br> Votre bracelet est confectionné dans notre atelier, en france, ensuite mis dans une pochette en soie, puis dans une boite en carton noire.
            <br>Nous l'envoyons ensuite dans une enveloppe bulle en lettre suivie. Vous recevez votre numéro de suivi via votre adresse mail automatiquement après l'envoi.</p>
    </main>
@endsection