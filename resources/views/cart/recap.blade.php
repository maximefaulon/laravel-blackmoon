@extends('layouts.app')

@section('title')
    Récapitulatif | BlackMoon
@endsection

@section('content')
    <main class="mt-4">
        <div class="container">
            <h1 class="display-4">Merci pour votre commande</h1>
            <p class="lead">Un email de confirmation a été envoyé et nous allons traiter votre commande dans les plus
                brefs délais.
                <br>Un suivi de votre commade sera disponible par email sous 2 jours ouvrés.</p>
        </div>
    </main>
@endsection

@section('scripts')
    <script>
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();

            if ('btoa' in window) {
                cvalue = btoa(cvalue);
            }
            document.cookie = cname + "=" + cvalue + "; " + expires + ';path=/';
        }

        setCookie('cartArticles', "", -1);
        setCookie('inCartItemsNum', "", -1);

    </script>
@endsection
