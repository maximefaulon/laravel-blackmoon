@extends('layouts.app')

@section('title')
    Informations commande | BlackMoon
@endsection

@section('content')
    <main class="mt-4">
        <div class="container">
            <h1 class="mt-3 display-4 text-center text-muted">Paiement</h1>
            <div class="row mt-5 justify-content-center">
                <div class="col-12 col-md-5 mt-md-5 mb-5 mb-md-0">
                    <p class="lead text-muted"><strong>Paiement avec ou sans compte paypal</strong> <br><br>L'inscription sur paypal est facultative</p>
                    <!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align=""><tbody><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/fr/webapps/mpp/paypal-popup" title="PayPal Comment Ca Marche" onclick="javascript:window.open('https://www.paypal.com/fr/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo-center/logo_paypal_paiements_securises_fr.jpg" border="0" alt="PayPal Acceptance Mark" /></a></td></tr></tbody></table><!-- PayPal Logo -->
                    <div id="paypal-button" class=""></div>
                </div>
                <div class="col-12 col-md-7">
                    <h3 class="display-6 text-muted text-center">Récapitulatif</h3>
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Produit</th>
                            <th scope="col">Prix</th>
                            <th scope="col">qt</th>
                            <th scope="col" colspan="2">Total</th>
                            {{--<th scope="col"></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $products as $product)
                            <tr>
                                <th>{{$product->name}}</th>
                                <th>{{$product->price}}€</th>
                                <th>{{$product->pivot->quantity}}</th>
                                <th>{{$product->price * $product->pivot->quantity}}€</th>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfooter>
                            <tr>
                                <th colspan="2"></th>
                                <th class="table-dark">Total</th>
                                <th class="table-dark subTotal" scope="col"></th>
                            </tr>
                        </tfooter>
                    </table>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script>
        paypal.Button.render({
            env: 'production', // Or 'production',

            commit: true, // Show a 'Pay Now' button

            style: {
                color: 'black',
                size: 'large'
            },

            payment: function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                return $.ajax({
                    type: "POST",
                    url: "{{ route('cart.paymentCheckout', [ 'cart_id' => $cart_id ]) }}",
                }).then(function (data) {
                    console.log(data.id);
                    return data.id;
                });
                {{--return paypal.request.post("{{ route('cart.paymentCheckout', [ 'id' => 1 ]) }}").then(function (data) {--}}
                {{--return data.id;--}}
                {{--});--}}
            },

            onAuthorize: function (data, actions) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.post("{{ route('cart.transaction', [ 'cart_id' => $cart_id ]) }}", {
                    'paymentID': data.paymentID,
                    'payerID': data.payerID,
                }).then(function (data) {
                    console.log(data.id);
                    document.location.href = "{{ route('cart.recap', [ 'cart_id' => $cart_id ]) }}"
                }).fail(function (error) {
                    console.log('erreur', error);
                });
                {{--return paypal.request.post("{{ route('cart.transaction', [ 'id' => 1 ]) }}", {--}}
                {{--paymentID: data.paymentID,--}}
                {{--payerID:   data.payerID,--}}
                {{--}).then(function(data) {--}}
                {{--console.log(data);--}}
                {{--document.location.href="{{ route('cart.recap', [ 'id' => 1 ]) }}"--}}
                {{--}).catch(function (error) {--}}
                {{--console.log('erreur', error);--}}
                {{--});--}}
            }
        }, '#paypal-button');
    </script>
@endsection