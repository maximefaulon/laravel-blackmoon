@extends('layouts.app')

@section('title')
    Panier | BlackMoon
@endsection

@section('content')
    <main>
        <div class="container mb-5">
            {{--<h1 class="display-4 ml-md-5">Panier</h1>--}}
            <div class="content-top p5">
                <h2>Panier</h2>
            </div>
            <div id="cart-without">
                <h1>Votre panier est vide</h1>
            </div>
            <div id="cart-with" class="row mt-5">
                <div class="col-12 col-md-9">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Produit</th>
                            <th scope="col">Prix</th>
                            <th scope="col">qt</th>
                            <th scope="col" colspan="2">Total</th>
                            {{--<th scope="col"></th>--}}
                        </tr>
                        </thead>
                        <tbody id="cartView">
                        {{--<tr>--}}
                            {{--<th class="align-middle" scope="row"><img src="http://lorempicsum.com/futurama/150/150/6" alt="">Cerbere</th>--}}
                            {{--<td class="align-middle">Mark</td>--}}
                            {{--<td class="align-middle">Otto</td>--}}
                            {{--<td class="align-middle">Otto</td>--}}
                            {{--<td>@mdo</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<th class="align-middle" scope="row">2</th>--}}
                            {{--<td class="align-middle">Jacob</td>--}}
                            {{--<td class="align-middle">Thornton</td>--}}
                            {{--<td>@fat</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<th class="align-middle" scope="row">3</th>--}}
                            {{--<td class="align-middle">Larry</td>--}}
                            {{--<td class="align-middle">the Bird</td>--}}
                            {{--<td>@twitter</td>--}}
                        {{--</tr>--}}
                        </tbody>
                    </table>
                </div>
                <div class="col-12 col-md-3">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" colspan="3">Total</th>
                            {{--<th scope="col"></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Sous total</th>
                            <td></td>
                            <td class="subTotal"></td>
                            {{--<td>@mdo</td>--}}
                        </tr>
                        <tr>
                            <th scope="row">Livraison</th>
                            <td></td>
                            <td>Offert</td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <td></td>
                            <td class="subTotal"></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('checkout') }}">
                        <button class="btn btn-dark btn--custom text-uppercase w-100">Commander</button>
                    </a>
                </div>
            </div>
        </div>
    </main>
@endsection