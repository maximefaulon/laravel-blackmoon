@extends('layouts.app')

@section('title')
    Informations commande | BlackMoon
@endsection

@section('content')
    <main class="mt-4">
        <div class="container">
            <div class="content-top p5">
                <h2>Coordonées</h2>
            </div>
            {!! form_start($form) !!}
            <div class="row justify-content-center">
                <div class="col-10 col-md-5">
                    {!! form_row($form->email) !!}
                    {!! form_row($form->bill_address->name) !!}
                    {!! form_row($form->bill_address->firstname) !!}
                    {!! form_row($form->bill_address->route) !!}
                    {!! form_row($form->bill_address->postalCode) !!}
                    {!! form_row($form->bill_address->city) !!}
                    {!! form_row($form->bill_address->country) !!}
                    {!! form_row($form->bill_address->phone) !!}
                    {{--<label for="delivery_check" class="lead">Adresse de livraison différente ?</label>--}}
                    {{--<input class="custom-control-input" type="checkbox" name="" id="delivery_check">--}}
                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="delivery_check">
                        <label class="custom-control-label" for="delivery_check">Adresse de livraison différente ?</label>
                    </div>
                    <div id="form_delivery">
                        {!! form_row($form->delivery_address->name, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->firstname, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->route, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->postalCode, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->city, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->country, $options = ['attr' => ['class' => 'form-control delivery']]) !!}
                        {!! form_row($form->delivery_address->phone, $options = ['attr' => ['class' => 'form-control delivery']]) !!}

                    </div>
                    {!! form_row($form->note) !!}
                </div>
                <div class="col-10 col-md-5 offset-md-1">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" colspan="3">Total</th>
                            {{--<th scope="col"></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Sous total</th>
                            <td></td>
                            <td class="subTotal"></td>
                            {{--<td>@mdo</td>--}}
                        </tr>
                        <tr>
                            <th scope="row">Livraison</th>
                            <td></td>
                            <td>Offert</td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <td></td>
                            <td class="subTotal"></td>
                        </tr>
                        </tbody>
                    </table>
                    {!! form_row($form->submit, $options = [ 'attr' => ['class' => 'btn btn-dark btn--custom w-100 text-uppercase']]) !!}
                </div>
            </div>
            {!! form_end($form) !!}
        </div>
    </main>
@endsection