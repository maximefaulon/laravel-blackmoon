<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Erreur - BlackMoon</title>

    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>
<main>
    <div class="text-center mt-5"><a href="{{route('home')}}" class="display-3">BlackMoon</a></div>
    <h1 class="display-4 text-center mt-3">Une erreur est survenue ! </h1>
</main>
</body>
</html>