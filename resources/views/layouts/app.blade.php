{{--<!DOCTYPE html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
{{--<head>--}}
{{--<meta charset="utf-8">--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

{{--<!-- CSRF Token -->--}}
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

{{--<title>@yield('title')</title>--}}
{{--<link rel="icon" type="image/png" href="/images/blackmoon2.png">--}}
{{--<!-- Styles -->--}}
{{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">--}}
{{--<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div id="app">--}}
{{--<div class="example">--}}
{{--<nav class="nav--main">--}}
{{--<a class="nav__logo" href="#">--}}
{{--BlackMoon--}}
{{--</a>--}}
{{--<ul class="nav__content">--}}
{{--<li><a href="">Bracelets</a></li>--}}
{{--<li><a href="">A propos</a></li>--}}
{{--<li><a href="">Contact</a></li>--}}
{{--</ul>--}}
{{--</nav>--}}
{{--<img src="http://via.placeholder.com/350x150" alt="">--}}
{{--</div>--}}
{{--<div>--}}
{{--<nav class="navbar navbar-expand-lg navbar-dark bg-dark p-3">--}}
{{--<a class="navbar-brand text-uppercase" href="{{ route('home') }}">Blackmoon</a>--}}
{{--<div class="d-flex d-md-none">--}}
{{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"--}}
{{--aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--<span class="navbar-toggler-icon"></span>--}}
{{--</button>--}}
{{--<a id="btnCart" class="text-uppercase btnCart">--}}
{{--<button class="btn btn-secondary">Panier <span class="badge badge-secondary"></span></button>--}}
{{--</a>--}}
{{--</div>--}}
{{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"--}}
{{--aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--<span class="navbar-toggler-icon"></span>--}}
{{--</button>--}}
{{--<div class="collapse navbar-collapse" id="navbarNav">--}}
{{--<ul class="navbar-nav">--}}
{{--<li class="nav-item drp--category">--}}
{{--<a class="nav-link" href="{{ route('bracelets') }}">Nos bracelets</a>--}}
{{--<div class="drp__content row">--}}
{{--<div class="col-6 p-4">--}}
{{--<h2 class="font-weight-light">Catégories</h2>--}}
{{--<a class="lead d-block" href="{{ route('bracelets', [ 'id_category' => 'ancre']) }}">Bracelet ancre</a>--}}
{{--<a class="lead d-block" href="{{ route('bracelets', [ 'id_category' => 'mousqueton']) }}">Bracelet mousqueton</a>--}}
{{--<a class="lead d-block" href="{{ route('bracelets', [ 'id_category' => 'queue de baleine']) }}">Bracelet queue de baleine</a>--}}
{{--</div>--}}
{{--<div class="col-6">--}}
{{--<img src="/images/accueil-bm.jpg" alt="" class="w-100">--}}
{{--</div>--}}
{{--</div>--}}
{{--</li>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('about') }}">A propos</a>--}}
{{--</li>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('contact') }}">Contact</a>--}}
{{--</li>--}}
{{--<li class="nav-item d-md-none">--}}
{{--<a class="nav-link" href="{{ route('cart') }}">Panier</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<a id="btnCart" class="text-uppercase d-none d-md-block btnCart">--}}
{{--<button class="btn btn-secondary">Panier <span class="badge badge-secondary"></span></button>--}}
{{--</a>--}}
{{--</nav>--}}

{{--<nav id="mySidenav" class="sidenav">--}}
{{--<a id="closeCart" href="javascript:void(0)" class="closebtn">&times;</a>--}}
{{--<div id="cart">--}}

{{--</div>--}}
{{--<footer class="mt-4 text-center">--}}
{{--<a href="{{ route('cart') }}" class="">--}}
{{--<button class="btn btn-primary text-uppercase w-75">Voir panier</button>--}}
{{--</a>--}}
{{--</footer>--}}
{{--</nav>--}}

{{--@yield('content')--}}
{{--</div>--}}
{{--<footer class="mt-3">--}}
{{--<div class="container-fluid text-center p-3 text-muted">--}}
{{--<p class="mb-1">2017-2018 BlackMoon</p>--}}
{{--<ul class="list-inline">--}}
{{--<li class="list-inline-item"><a href="{{ route('termsOfSales') }}">CGV</a></li>--}}
{{--<li class="list-inline-item"><a href="{{ route('contact') }}">Contact</a></li>--}}
{{--<li class="list-inline-item"><a href="{{ route('paymentInfo') }}">Paiement</a></li>--}}
{{--<li class="list-inline-item"><a href="{{ route('deliveryInfo') }}">Livraison</a></li>--}}
{{--</ul>--}}

{{--</div>--}}
{{--</footer>--}}

{{--<!-- Scripts -->--}}
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
{{--@yield('scripts')--}}
{{--</body>--}}
{{--</html>--}}


        <!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('title')</title>
    @yield('meta')
    <meta name="google-site-verification" content="zjiaosgsysqOTPto6gxzlahWsJoxbeQjbjPB6GA1Dbo" />
    <meta name="google-site-verification" content="OxGThPIAmOyHD7kfEOUkkV0Ua7GTHvzbWwBo09U6bHM" />
    <link rel="icon" type="image/png" href="{{ asset('images/bleueisland-favicon.png') }}">
    <link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css'/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118195869-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-118195869-2');
    </script>



    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!--<script src="js/jquery.easydropdown.js"></script>-->
    <!--start slider -->
    {{--<link rel="stylesheet" href="{{ asset('css/fwslider.css') }}" media="all">--}}
    <link type="text/css" rel="stylesheet" href="{{ asset('css/lightslider.css') }}"/>
{{--<script src="{{ asset('js/jquery-ui.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/fwslider.js') }}"></script>--}}
<!--end slider -->

</head>
<body>
{{--<div class="pre-header"></div>--}}
<header>
    <div class="container-fluid pt-4">
        <div class="row no-gutters text-center">
            <div class="col-2">
                <a id="collapse-link" class="collaple__toggler" href="#"><i class="fas fa-bars fa-2x"></i></a>
            </div>
            <div class="col-8">
                <a href="{{ route('home') }}">
                    <img class="navbar__logo" src="{{ asset('images/bleueislandv6.png') }}" alt="Bracelet bleue island logo">
                </a>
            </div>
            <div class="col-2">
                <a rel="nofollow" class="font basket__link" href="{{ route('cart') }}"><img src="{{ asset('images/coconut-drink.svg') }}" width="40px" alt="logo panier bleueisland"> Panier</a>
                {{--<div class="cart-view">--}}
                    {{--<div id="cart"></div>--}}
                {{--</div>--}}
                {{--<div id="mySidenav" class="sidenav">--}}
                    {{--<a href="javascript:void(0)" id="closeCart" class="closebtn">&times;</a>--}}
                    {{--<div id="cart"></div>--}}
                {{--</div>--}}
            </div>
        </div>
        <nav class="mt-4 mb-2">
            {{--<ul>--}}
                {{--<li><a href="{{ route('bracelets') }}">Bracelets</a></li>--}}
                {{--<li><a href="{{ route('about') }}">A propos</a></li>--}}
                {{--<li><a href="{{ route('contact') }}">Contact</a></li>--}}
            {{--</ul>--}}



            <ul class="nav--main">
                <li class=""><a href="{{ route('home') }}"><i class="fas fa-home"></i> Accueil</a></li>
                <li><a href="{{ route('bracelets') }}">Tous nos bracelets</a></li>
                <li><a href="{{ route('bracelets', [ 'id_category' => 'ancre' ]) }}">Bracelets ancre</a></li>
                <li><a href="{{ route('bracelets', [ 'id_category' => 'queue de baleine' ]) }}">Bracelets queue de baleine</a></li>
                <li><a href="{{ route('bracelets', [ 'id_category' => 'mousqueton' ]) }}">Bracelets mousqueton</a></li>
                <li><a href="{{ route('about') }}">A propos</a></li>
                <li><a rel="nofollow" href="{{ route('contact') }}">Contact</a></li>
            </ul>


        </nav>
    </div>
</header>

{{--<header style="">--}}
    {{--<nav class="navbar--main">--}}
        {{--<div class="navbar__content--logo">--}}
            {{--<img class="navbar__logo" src="../images/logo-moon.png" alt="">--}}
        {{--</div>--}}
        {{--<a class="collaple__toggler" href="#"><i class="fas fa-bars fa-2x"></i></a>--}}
        {{--<div class="collapse__content">--}}
            {{--<ul>--}}
                {{--<li><a href="">Bracelets</a></li>--}}
                {{--<li><a href="">A propos</a></li>--}}
                {{--<li><a href="">Contact</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</nav>--}}
{{--</header>--}}
{{--<header style="margin-top: 500px;" class="">--}}
{{--<nav class="navbar navbar-expand-lg navbar-dark bg-primary px-5">--}}
{{--<a class="navbar-brand text-uppercase p-2 position-relative" style="font-size: 30px" href="#">--}}
{{--<img src="../images/moon.svg" class="logo--headernav" width="60px" alt="">--}}
{{--<p class="position-relative" style="z-index: 2; color: #ffffff; font-weight: 800;">blackmoon</p>--}}
{{--</a>--}}
{{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"--}}
{{--aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--<span class="navbar-toggler-icon"></span>--}}
{{--</button>--}}

{{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--<ul class="navbar-nav mr-auto">--}}
{{--<li class="nav-item p-2 dropdown">--}}
{{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"--}}
{{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--Bracelets--}}
{{--</a>--}}
{{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
{{--<a class="dropdown-item" href="#">Ancre</a>--}}
{{--<a class="dropdown-item" href="#">Queue de baleine</a>--}}
{{--<div class="dropdown-divider"></div>--}}
{{--<a class="dropdown-item" href="#">Mousqueton</a>--}}
{{--</div>--}}
{{--</li>--}}
{{--<li class="nav-item p-2">--}}
{{--<a class="nav-link" href="#">A propos</a>--}}
{{--</li>--}}
{{--<li class="nav-item p-2">--}}
{{--<a class="nav-link" href="#">Contact</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<li class="nav-item p-2 nav-cart">--}}
{{--<a class="nav-link" href="#">Panier</a>--}}
{{--</li>--}}
{{--<button class="btn btn-link">Panier</button>--}}
{{--</div>--}}
{{--</nav>--}}
{{--</header>--}}
{{--<div class="header">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-md-12">--}}
{{--<div class="header-left">--}}
{{--<div class="logo">--}}
{{--<a href="{{ route('home') }}">--}}
{{--<img src="{{ asset('images/moon.svg') }}" width="62px" alt=""/>--}}
{{--</a>--}}
{{--</div>--}}
{{--<div class="menu">--}}
{{--<a class="toggleMenu" href="#"><img src="{{ asset('images/nav.png') }}" alt="" /></a>--}}
{{--<ul class="nav" id="nav">--}}
{{--<li><a href="{{route('bracelets')}}">Bracelets</a></li>--}}
{{--<li><a href="team.html">Team</a></li>--}}
{{--<li><a href="{{ route('about') }}">A propos</a></li>--}}
{{--<li><a href="{{ route('contact') }}">Contact</a></li>--}}
{{--<li><a href="shop.html">Company</a></li>--}}
{{--<li><a href="contact.html">Contact</a></li>--}}
{{--<div class="clear"></div>--}}
{{--</ul>--}}
{{--<script type="text/javascript" src="{{ asset('js/responsive-nav.js') }}"></script>--}}
{{--</div>--}}
{{--<div class="clear"></div>--}}
{{--</div>--}}
{{--<div class="header_right">--}}
{{--<ul class="icon1 sub-icon1 profile_img">--}}
{{--<li><a class="active-icon c1" href="#"> </a>--}}
{{--<ul class="sub-icon1 list">--}}
{{--<div id="cart"></div>--}}
{{--<a href="{{ route('cart') }}">--}}
{{--<button class="w-100 btn btn-dark btn--custom">Voir le panier</button>--}}
{{--</a>--}}
{{--</ul>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

<div style="min-height: 500px;">
    @yield('content')
</div>

<div class="footer">
    <div class="container">
        <div class="row">
            {{--<div class="col-md-3">--}}
            {{--<ul class="footer_box">--}}
            {{--<h4>Products</h4>--}}
            {{--<li><a href="#">Mens</a></li>--}}
            {{--<li><a href="#">Womens</a></li>--}}
            {{--<li><a href="#">Youth</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            <div class="col-md-3 offset-0 offset-md-3">
                <ul class="footer_box">
                    <h4>A propos</h4>
                    {{--<li><a href="#">Careers and internships</a></li>--}}
                    <li><a href="{{ route('about') }}">L'équipe</a></li>
                    <li><a href="{{ route('termsOfSales') }}">CGV</a></li>
                    {{--<li><a href="#">Catalog Request/Download</a></li>--}}
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer_box">
                    <h4>Support</h4>
                    <li><a rel="nofollow" href="{{ route('contact') }}">Contact</a></li>
                    <li><a href="{{ route('deliveryInfo') }}">Livraison</a></li>
                    {{--<li><a href="#">Easy Returns</a></li>--}}
                    <li><a href="{{ route('paymentInfo') }}">Paiement</a></li>
                    {{--<li><a href="#">Replacement Binding Parts</a></li>--}}
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer_box">
                    {{--<h4>Newsletter</h4>--}}
                    {{--<div class="footer_search">--}}
                    {{--<form>--}}
                    {{--<input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">--}}
                    {{--<input type="submit" value="Go">--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    <ul class="social">
                        <li><a rel="nofollow" href="https://www.facebook.com/BlackMoon-1874425259254500/"><i class="fab fa-facebook fa-3x"></i></a>
                        </li>
                        {{--<li class="twitter"><a href="#"><span> </span></a></li>--}}
                        <li><a rel="nofollow" href="https://www.instagram.com/bleueisland/"><i class="fab fa-instagram fa-3x"></i></a>
                        </li>
                        {{--<li class="pinterest"><a href="#"><span> </span></a></li>--}}
                        {{--<li class="youtube"><a href="#"><span> </span></a></li>--}}
                    </ul>

                </ul>
            </div>
        </div>
        <div class="row footer_bottom justify-content-center">
            <div class="copy">
                <p>©2017 Bleue Island</p>
            </div>
            {{--<dl id="sample" class="dropdown">--}}
            {{--<dt><a href="#"><span>Change Region</span></a></dt>--}}
            {{--<dd>--}}
            {{--<ul>--}}
            {{--<li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>--}}
            {{--<li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>--}}
            {{--<li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>--}}
            {{--<li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>--}}
            {{--<li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>--}}
            {{--<li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>--}}
            {{--</ul>--}}
            {{--</dd>--}}
            {{--</dl>--}}
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>