@extends('layouts.app')

@section('title')
    {{ $product->nameref }} | {{ $product->name }}
@endsection

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">BlackMoon</a></li>
            <li class="breadcrumb-item"><a href="#">Bracelets</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
        </ol>
    </nav>
    <main class="container-fluid mb-5">
        <div class="row pt-3 justify-content-center">
            <div class="col-12 col-md-5 offset-md-1">
                <img class="img-responsive w-100" src="{{ asset($product->pictures[0]->path) }}" alt="{{ $product->pictures[0]->alt }}">
            </div>
            <div class="col-12 col-md-6 text-center mt-5">
                <h1 class="lead">{{ $product->nameref }}</h1>
                <h2 class="display-4">{{ $product->name}}</h2>
                <p class="lead text-muted">{{$product->price}}€</p>
                <div class="row justify-content-center mt-3">
                <div class="col-5 align-content-center">
                    <input id="input-qt" class="form-control w-100" type="number" min="1" value="1">
                </div>
                </div>
                <div class="row justify-content-center mt-5 mb-5">
                    <div class="col-5 align-content-center">
                        <select class="custom-select w-100" name="" id="select-size">
                            <option value="S">S - (15cm - 18,5cm)</option>
                            <option value="M">M - (18,5cm - 21cm)</option>
                        </select>
                    </div>
                </div>

                <button class="btn btn-dark btn--custom text-uppercase add-to-cart"
                        data-name="{{ $product->name }}"
                        data-url="{{ asset($product->pictures[0]->path) }}"
                        data-price="{{ $product->price }}">Ajouter au panier</button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-md-5 offset-md-1">
                <h3 class="display-4">Description</h3>
                <p class="lead">
                    {!! str_replace(PHP_EOL, '<br />', $product->description) !!}
                </p>
            </div>
        </div>
    </main>
@endsection