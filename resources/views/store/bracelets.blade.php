@extends('layouts.app')

@section('title')
    {{ $name }} homme | Bracelet homme acier inoxydable
@endsection

@section('meta')
    <meta name="description" content="{!! $description !!}" />
@endsection

@section('content')
    <main class="mb-5">

        {{--<nav aria-label="breadcrumb">--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li class="breadcrumb-item"><a href="#">BlackMoon</a></li>--}}
                {{--<li class="breadcrumb-item"><a href="#">Bracelets</a></li>--}}
            {{--</ol>--}}
        {{--</nav>--}}
        {{--<hr style="width: 50%">--}}

        {{--<div class="container-fluid mt-3">--}}
            {{--<div class="row no-gutters justify-content-center hero--bracelet">--}}
                {{--<div class="col-12 p-4 hero__content">--}}
                    {{--<h1 class="display-4 text-center">{!! $name !!}</h1>--}}
                    {{--<p class="lead text-center">{!! $description !!}</p>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-md-7">--}}
                    {{--<img class="rounded img-responsive w-100" src="http://lorempicsum.com/futurama/1000/300/2" alt="">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="header--page">--}}
            {{--<h1>{!! $name !!}</h1>--}}
            {{--<p>{!! $description !!}</p>--}}
        {{--</div>--}}
        {{--<div class="container-fluid">--}}
        {{--<div class="row no-gutters bg-light">--}}
            {{--<div class="img--bracelets col-12 col-md-7 order-md-0 order-1">--}}
                {{--<img src="../images/featured-image-index.jpg" class="w-100" alt="">--}}
            {{--</div>--}}
            {{--<div class="col-12 col-md-5 p-3 text-center">--}}
                {{--<h1>{!! $name !!}</h1>--}}
                {{--<p>{!! $description !!}</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="mt-4">
            {{--<h1 class="display-4 text-center">Toute la collection</h1>--}}
            <div style="background-color: #f5f6fa;" class="text-center rounded p-3">
                <h1 style="font-size: 30px;" class="font-weight-light text-uppercase">
                    {!! $name !!}
                </h1>
                <h2 class="lead">{!! $name !!} homme acier inoxydable</h2>
                <p class="container font-weight-light text-muted">{!! $description !!}</p>
            </div>
        </div>
            <div class="container mt-2">
                <div class="row justify-content-center justify-content-md-start">
                    {{--Boucle produits--}}
                @foreach( $products as $product)
                    <div class="col-12 col-md-3 mb-3">
                        <a href="{{ route('product', [ 'id' => $product->name]) }}">
                            <img class="img-responsive text-center" style="width: 100%" src="{{ asset($product->pictures[0]->path) }}" alt="{{ $product->pictures[0]->alt }}">
                            {{--<h2 style="font-size: 16px;" class="text-muted font-weight-light">{{ $product->name }}- Bracelet queue de baleine - noir</h2>--}}
                            <h3 style="font-size: 16px;" class="text-muted font-weight-light">{{ $product->name }}- {{ $product->nameref }}</h3>
                            <p style="font-size: 18px" class="text-muted">{{ $product->price }}€</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection