
inCartItemsNum = parseInt(getCookie('inCartItemsNum') ? getCookie('inCartItemsNum') : 0);
cartArticles = getCookie('cartArticles') ? JSON.parse(getCookie('cartArticles')) : [];

$('#cart-with').hide();
$('#cart-without').hide();
$(".loader").hide();

/*$(window).load( function () {
    $('.loader').show();
});

$(window).onloadend( function () {
    $('.loader').hide();
});*/

/*$(window).( function () {
    $('.loader').hide();
});*/

$(document).ready(function(){

if (window.location.pathname == "/cart") {
    console.log(1)
    var items = '';
    var subTotal = 0;
    var total;

    if (inCartItemsNum > 0) {

        // $('#cart-without').hide();
        $('#cart-with').show();


            cartArticles.forEach(function(v) {
                var itemPrice = v.price;
                item = `<tr ref="`+ v.name + v.size +`">
                            <td class="align-middle">
                                <!--<div class="cell medium-6">-->
                                    <img src="`+ baseUrlImg + v.url +`" width="120px">
                                <!--</div>-->
                                <!--<div class="cell medium-6 grid-x">-->
                                    <p id="size" class="margin-right-2">`+ v.size +`</p>
                                    <p>`+ v.name +`</p>
                                <!--</div>-->
                            </td>
                            <td class="align-middle">`+ v.price +`€</td>
                            <td class="align-middle">`+ v.qt +`</td>
                            <td class="align-middle">`+ v.qt * v.price +`€</td>
                            <td class="delete-item"><i class="fas fa-times"></i></td>
                        </tr>`;

                item = item.replace("url", v.url);
                items+= item;
                subTotal += v.price * v.qt;


            });

        $('#cartView').empty().html(items);
        $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
    }
    else
    {
        $('#cart-without').show();
    }

    $('input').on('change', function () {
        // $('input').prop("disabled", true);
        var qt = $(this).val();
        var size = $('#sizeValue').val();
        // console.log(size);
        subTotal = 0;
        // var subTotal = parseInt($('.subTotal').val());
        var ref = $(this).attr('ref');
        cartArticles.forEach(function(v) {
            if (v.id + v.size == ref) {
                v.qt = qt;
            }
            subTotal += v.price * v.qt;
        });
        $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
        saveCart(inCartItemsNum, cartArticles);
        cartEmptyToggle();
        $('input').attr("disabled", false);
    });

}



    $(".delete-item").click( function() {
        var id = $(this).parent().attr('ref');
        // var qt = $("[ref="+id+"] .input_qt").val();
        // var size = $("[ref="+id+"] #size").val();
        // var size = document.getElementById("select_size-").value;
        // console.log(size);

        var arrayId = 0;
        var price;

        // supprime l'item du DOM
        $(this).parent().hide(600);
        $('#'+ id).hide(600);
        // $('#'+ id).remove();
        //
        cartArticles.forEach(function(v) {
            // on récupère l'id de l'article dans l'array
            // if (v.id == id & v.size == size) {
            if (v.id + v.size == id) {
                inCartItemsNum -= 1;
                // on met à jour le sous total et retire l'article de l'array
                // as usual, calcul sur des entiers
                var itemPrice = v.price.replace(',', '.') * 1000;
                subTotal -= (itemPrice * v.qt) / 1000;
                //weight -= artWeight * qt;
                cartArticles.splice(arrayId, 1);

                return false;
            }
            arrayId++;
        });

        $('.badge').html(inCartItemsNum);
        $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
        //$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
        saveCart(inCartItemsNum, cartArticles);
        cartEmptyToggle();

        if(inCartItemsNum == 0) {
            $('#cart-with').hide();
            $('#cart-without').show();
        }
    });


if (window.location.pathname == "/cart/checkout") {
    $('#delivery-address').hide();
    $('document').ready(function () {
        $('#delivery').change( function () {
            if($('#delivery').is(':checked')){
                $('#delivery-address').animate({
                    height: 'toggle'
                });

            }
            else {
                $('#delivery-address').hide();
            }
        });

    });

}



});