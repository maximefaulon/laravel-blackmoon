/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// window.$ = window.jQuery = require('jquery');
require('./jquery.min');
require('./jquery-ui.min');
require('./jquery.flexisel');
// require('./bootstrap');
require('./lightslider');
var Instafeed = require('./instafeed');
// require('./fwslider');
// require('./index');
require('./cart');
// require('./cartview');


// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

$('.cart-link').bind('click', function (e) {
    e.preventDefault();
    // document.getElementById("mySidenav").style.width = "400px";
    $('#mySidenav').addClass('sidenav-show');
    // document.getElementById("main").style.marginLeft = "250px";
});

$('#closeCart').bind('click', function () {
    // document.getElementById("mySidenav").style.width = "0";
    $('#mySidenav').removeClass('sidenav-show');
    // document.getElementById("mySidenav").style.width = "0";
    // document.getElementById("main").style.marginLeft = "0";
});


$(document).ready(function () {
    if (window.innerWidth < 767 ) {
        $("#lightSlider").lightSlider({
            item: 3,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,

            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////

            speed: 400, //ms'
            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,

            keyPress: false,
            controls: true,
            prevHtml: '',
            nextHtml: '',

            rtl: false,
            adaptiveHeight: false,

            vertical: false,
            verticalHeight: 500,
            vThumbWidth: 100,

            thumbItem: 10,
            pager: true,
            gallery: false,
            galleryMargin: 5,
            thumbMargin: 5,
            currentPagerPosition: 'middle',

            enableTouch: true,
            enableDrag: true,
            freeMove: true,
            swipeThreshold: 40,

            responsive: [],
        });
    }
    else {
        $("#lightSlider").lightSlider({
            item: 4,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,

            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////

            speed: 400, //ms'
            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,

            keyPress: false,
            controls: true,
            prevHtml: '',
            nextHtml: '',

            rtl: false,
            adaptiveHeight: false,

            vertical: false,
            verticalHeight: 500,
            vThumbWidth: 100,

            thumbItem: 10,
            pager: true,
            gallery: false,
            galleryMargin: 5,
            thumbMargin: 5,
            currentPagerPosition: 'middle',

            enableTouch: true,
            enableDrag: true,
            freeMove: true,
            swipeThreshold: 40,

            responsive: [],
        });
    }


$('#collapse-link').bind('click', function (e) {
    e.preventDefault();
    $('.nav--main').show();

});
    var feed = new Instafeed({
        get: 'user',
        // tagName: 'love',
        userId: '7242897447',
        accessToken: '7242897447.b17a5a5.7d403ef9d3f94ef7ae043f4740c0c4fb',
        resolution: 'standard_resolution',
        template: '<a href="{{link}}"><img class="img-responsive thumbnail--insta" src="{{image}}" /></a>',
        limit: 8
    });
    feed.run();

});





