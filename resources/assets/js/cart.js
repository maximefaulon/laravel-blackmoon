import $ from 'jquery';

var baseUrlImg = "";
var baseUrllink = "/catalog/productSheet/";
// var baseUrlCatalog = "/Symfony/web/app_dev.php/catalog/";

/*function AjaxCallProduct(id) {
    var price = 0;
    $.ajax({
        url: "/Symfony/web/app_dev.php/catalog/product/getPrice",
        method: "post",
        data: { id : id}
    }).done(function(msg) {
        console.log(msg);
        price = msg;
    });
    return price;
}*/


function setCookie(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    if ('btoa' in window) {
        cvalue = btoa(cvalue);
    }
    document.cookie = cname + "=" + cvalue + "; " + expires + ';path=/';
}

function saveCart(inCartItemsNum, cartArticles) {
    setCookie('inCartItemsNum', inCartItemsNum, 1);
    setCookie('cartArticles', JSON.stringify(cartArticles), 1);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c[0] == ' ') {
            c = c.substring(1);
        }

        if (c.indexOf(name) != -1) {
            if ('btoa' in window) {
                return atob(c.substring(name.length, c.length));
            }
            else {
                return c.substring(name.length, c.length);
            }
        }
    }
    return false;
}


var inCartItemsNum = 0;
var cartArticles;

function cartEmptyToggle() {
    // $('#no-empty-cart').hide();
    if (inCartItemsNum > 0) {
        // $('#btn-commander').removeClass('hidden');
        // $('#lien-panier').click(function(){
        //     return window.location='/panier.php';
        // });
        $('#no-empty-cart').show();
        $('#empty-cart').hide()
    }
    else {
        // $('#lien-panier').click(function(){
        //     return false;
        // });
        $('#no-empty-cart').hide();
        $('#empty-cart').show();
    }
}

inCartItemsNum = parseInt(getCookie('inCartItemsNum') ? getCookie('inCartItemsNum') : 0);
cartArticles = getCookie('cartArticles') ? JSON.parse(getCookie('cartArticles')) : [];

if (cartArticles == null) {
    inCartItemsNum = 0;
    cartArticles = [];
    saveCart(inCartItemsNum, cartArticles);
}
if (inCartItemsNum < 0 | cartArticles.length == 0) {
    inCartItemsNum = 0;
    cartArticles = [];
    saveCart(inCartItemsNum, cartArticles);
}

console.log(cartArticles)


cartEmptyToggle();
$('.badge').html(inCartItemsNum);


var items = '';
var item;
var subTotal = 0;
cartArticles.forEach(function (v) {
    subTotal += parseInt(v.price) * v.qt;
    /*.replace(',', '.') * 1000;*/
    item = `<a href="/product/` + v.name + `">
                <div class="row" id="'+ v.name + v.size +'" ref="`+ v.name +`">
                    <div class="col-6">
                        <img src="` + baseUrlImg + v.url + `"  class="" width="100px" height="100px">
                    </div>
                    <div class="col-6">
                        <p class=""><strong class="">` + v.name + `</strong></p><p class="">x<span id="qt-` + v.name + v.size + `" class="qt">` + v.qt + `</span></p>
                    </div>
                </div>
            </a>`;
    items += item;
});

$('.subTotal').html(subTotal.toFixed(2) + '€');
$('#cart').prepend(items);

$(document).ready(function () {

    $('.add-to-cart').click(function () {
        console.log(cartArticles)
        var $this = $(this);
        var name = $this.attr('data-name');
        //
        //     $.ajax({
        //         url: "/catalog/product/getPrice",
        //         method: "post",
        //         data: { id : id}
        //     }).done(function(msg) {
        //
        //
            var size = document.getElementById("select-size").value;
            if(!size) {
                window.alert('Veuillez sélectionner une taille');
                return false;
            }
        // var price = msg;
        // var price = 0;
        var price = $this.attr('data-price');
        var url = $this.attr('data-url');
        var qt = parseInt(document.getElementById("input-qt").value);

        var newArticle = true;
        subTotal += parseInt(price);

        cartArticles.forEach(function (v) {
            // var req;
            if (v.name == name & v.size == size) {
                newArticle = false;
                v.qt += qt;
                $('#qt-' + name + size).html(v.qt);
            }
        });
        if (newArticle) {
             var req = `<a href="/product/` + name + `">
                        <div class="row" id="'+ v.name + v.size +'" ref="`+ name +`">
                            <div class="col-6">
                                <img src="` + baseUrlImg + url + `"  class="" width="100px" height="100px">
                            </div>
                            <div class="col-6">
                                <p class=""><strong class="">` + name + `</strong></p><p class="">x<span id="qt-` + name + size + `" class="qt">` + qt + `</span></p>
                            </div>
                        </div>
                    </a>`;
            $('#cart').prepend(req);

            cartArticles.push({
                name: name,
                price: price,
                size: size,
                qt: qt,
                url: url
            });
            inCartItemsNum += 1;
        }

        /*$('.alert').show();*/

        saveCart(inCartItemsNum, cartArticles);
        $('.subTotal').html(subTotal.toFixed(2) + '€');
        $('.badge').html(inCartItemsNum);

        cartEmptyToggle();
        var alert = `<div class="alert alert-info alert-dismissible fade show" role="alert">
                    <strong>Bravo !</strong> Article ajouté avec succès.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>`;

        $('main').prepend(alert);
        $('html, body').animate({scrollTop: 0}, 'slow');

        // });
    });
});



$(document).ready(function(){

    if (window.location.pathname == "/cart") {
        console.log(1)
        var items = '';
        var subTotal = 0;
        var total;

        if (inCartItemsNum > 0) {

            $('#cart-without').hide();
            $('#cart-with').addClass('cart-with')


            cartArticles.forEach(function(v) {
                var itemPrice = v.price;
                item = `<tr ref="`+ v.name + v.size +`">
                            <td class="align-middle">
                                <!--<div class="cell medium-6">-->
                                    <img src="`+ baseUrlImg + v.url +`" width="120px">
                                <!--</div>-->
                                <!--<div class="cell medium-6 grid-x">-->
                                    <p id="size" class="margin-right-2">`+ v.size +`</p>
                                    <p>`+ v.name +`</p>
                                <!--</div>-->
                            </td>
                            <td class="align-middle">`+ v.price +`€</td>
                            <td class="align-middle">`+ v.qt +`</td>
                            <td class="align-middle">`+ v.qt * v.price +`€</td>
                            <td class="align-middle delete-item"><i class="fas fa-times"></i></td>
                        </tr>`;

                item = item.replace("url", v.url);
                items+= item;
                subTotal += v.price * v.qt;


            });

            $('#cartView').empty().html(items);
            $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
        }
        else
        {
            $('#cart-without').show();
        }

        $('input').on('change', function () {
            // $('input').prop("disabled", true);
            var qt = $(this).val();
            var size = $('#sizeValue').val();
            // console.log(size);
            subTotal = 0;
            // var subTotal = parseInt($('.subTotal').val());
            var ref = $(this).attr('ref');
            cartArticles.forEach(function(v) {
                if (v.id + v.size == ref) {
                    v.qt = qt;
                }
                subTotal += v.price * v.qt;
            });
            $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
            saveCart(inCartItemsNum, cartArticles);
            cartEmptyToggle();
            $('input').attr("disabled", false);
        });

    }



    $(".delete-item").click( function() {
        var id = $(this).parent().attr('ref');
        // var qt = $("[ref="+id+"] .input_qt").val();
        // var size = $("[ref="+id+"] #size").val();
        // var size = document.getElementById("select_size-").value;
        // console.log(size);

        var arrayId = 0;
        var price;

        // supprime l'item du DOM
        $(this).parent().hide(600);
        $('#'+ id).hide(600);
        // $('#'+ id).remove();
        //
        cartArticles.forEach(function(v) {
            // on récupère l'id de l'article dans l'array
            // if (v.id == id & v.size == size) {
            if (v.name + v.size == id) {
                inCartItemsNum -= 1;
                // on met à jour le sous total et retire l'article de l'array
                // as usual, calcul sur des entiers
                var itemPrice = v.price.replace(',', '.') * 1000;
                subTotal -= (itemPrice * v.qt) / 1000;
                //weight -= artWeight * qt;
                cartArticles.splice(arrayId, 1);

                return false;
            }
            arrayId++;
        });

        $('.badge').html(inCartItemsNum);
        $('.subTotal').html(subTotal.toFixed(2).replace('.', ',') + '€');
        //$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
        saveCart(inCartItemsNum, cartArticles);
        cartEmptyToggle();

        if(inCartItemsNum == 0) {
            $('#cart-with').removeClass('cart-with')
            $('#cart-without').show();
        }
    });


    if (window.location.pathname == "/cart/checkout") {
        // $('#form_delivery').hide();
        $('document').ready(function () {
            $('#delivery_check').change( function () {
                if($('#delivery_check').is(':checked')){
                    $('#form_delivery').animate({
                        height: 'toggle'
                    });
                    $('.delivery').attr('required', true);

                }
                else {
                    $('.delivery').attr('required', false);
                    $('#form_delivery').hide();
                }
            });

        });

    }



});





